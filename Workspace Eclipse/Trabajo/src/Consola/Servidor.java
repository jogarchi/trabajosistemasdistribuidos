package Consola;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Servidor 
{
	public static void main(String[] args) 
	{
		//Creamos un pool cuyo tama�o ir� variando seg�n se vayan iniciando o finalizando partidas
		ExecutorService pool=Executors.newCachedThreadPool();
		try(ServerSocket socket=new ServerSocket(6666);)
		{
			while(true)
			{
				//Aceptamos a ambos jugadores
				final Socket jugadorX=socket.accept();
				final Socket jugadorO=socket.accept();
				//Una vez que est�n aceptados los dos jugadores, iniciamos la partida
				Runnable r=new GestionarPartida(jugadorX, jugadorO);
				pool.execute(r);
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		pool.shutdown();
	}
}
