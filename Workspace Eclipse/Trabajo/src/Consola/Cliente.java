package Consola;

import java.io.*;
import java.net.*;


public class Cliente 
{
	public static void main(String[] args)
	{
		
		try(java.util.Scanner in=new java.util.Scanner(System.in);)
		{
			String simbolo,partida,partidaTerminada;
			int fila,columna,cont;
			
			@SuppressWarnings("resource")
			Socket client=new Socket(InetAddress.getLocalHost(), 6666);
			BufferedReader br= new BufferedReader(new InputStreamReader(client.getInputStream()));		//Creamos el buffer para leer lo que manda el cliente
			Writer w=new OutputStreamWriter(client.getOutputStream());		//Creamos el writer para enviar datos al cliente
			
			partidaTerminada=br.readLine();
			while(!partidaTerminada.equals("ok"))
			{
				// Muestra por pantalla con que simbolo tiene que jugar el jugador: si con X o con O
				simbolo = br.readLine();
				System.out.println(simbolo);

				// Muestra por pantalla el estado de la partida
				cont=0;
				while (cont<3) 
				{
					partida = br.readLine();
					System.out.println(partida);
					cont++;
				}

				// El jugador selecciona la posicion en la que quiere introducir su s�mbolo
				System.out.println("Introduce el numero de fila:\r\n");
				System.out.println("RECUERDA QUE LAS POSICIONES SON DE 0 A 2\r\n");	

				fila = in.nextInt();
				w.write(fila + "\r\n");
				w.flush();

				System.out.println("Introduce el numero de columna:\r\n");
				System.out.println("RECUERDA QUE LAS POSICIONES SON DE 0 A 2\r\n");
	
				columna = in.nextInt();
				w.write(columna + "\r\n");
				w.flush();
				partidaTerminada=br.readLine();
			}
			
			//Muestra cu�l ha sido el resultado de la partida
			System.out.println(br.readLine());
			cont=0;
			//Muestra c�mo ha acabado la partida
			while (cont<3) 
			{
				partida = br.readLine();
				System.out.println(partida);
				cont++;
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
