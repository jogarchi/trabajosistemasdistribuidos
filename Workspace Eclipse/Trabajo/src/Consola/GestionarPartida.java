package Consola;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

public class GestionarPartida implements Runnable {
	
	private Socket jugadorX;
	private Socket jugadorO;
	
	public GestionarPartida(Socket jugadorX,Socket jugadorO) {
		this.jugadorX=jugadorX;
		this.jugadorO=jugadorO;
	}

	public void run() {
		try(BufferedReader inX=new BufferedReader(new InputStreamReader(jugadorX.getInputStream()));
			Writer outX=new OutputStreamWriter(jugadorX.getOutputStream());
			BufferedReader inO=new BufferedReader(new InputStreamReader(jugadorO.getInputStream()));
			Writer outO=new OutputStreamWriter(jugadorO.getOutputStream());){
			
			String [][] matriz=new String[3][3];//Se crea una matriz para almacenar el "tablero" de la partida
			int i=0;
			int j=0;
			while(i<matriz.length)//Al principio el "tablero" empieza vacio 
			{
				while(j<matriz.length) {
					matriz[i][j]=" ";
					j++;
				}
				j=0;
				i++;
			}
			int cont=0;
			int turno=1;//Se controla el turno para saber a que jugador le toca en cada momento
			int fila,columna;
			boolean acabada=false;
			while((cont<9)&&!acabada)//Mientras que se pueden realizar jugadas y no hay un ganador se desarrolla la partida
			{
				if(turno%2!=0)//Un jugador realiza una jugada
				{
					outX.write("no\r\n");
					outX.write("Juegas con  X"+"\r\n");
					outX.write(mostrarMatriz(matriz));
					outX.flush();
					boolean posicionValida=false;
					
					while(!posicionValida)//Mientras que no se introduce una posicion valida se realiza este bucle
					{
						
						fila=Integer.parseInt(inX.readLine());
						columna=Integer.parseInt(inX.readLine());
						if(posicionValida(matriz, fila, columna))//Se comprueba que en la posici�n indicada se puede realizar la jugada
						{
							matriz[fila][columna]="X";
							posicionValida=true;
						}
						else
						{
							outX.write("no\r\n");
							outX.write("LA POSICION ANTERIOR NO ES V�LIDA. Sigues jugando con  X"+"\r\n");
							outX.write(mostrarMatriz(matriz));
							outX.flush();
						}
					}
					turno++;
				}
				else//El otro jugador realiza su jugada
				{
					outO.write("no\r\n");
					outO.write("Juegas con  O"+"\r\n");
					outO.write(mostrarMatriz(matriz));
					outO.flush();
					boolean posicionValida = false;

					while (!posicionValida)//Mientras que no se introduce una posicion valida se realiza este bucle
					{
						fila = Integer.parseInt(inO.readLine());
						columna = Integer.parseInt(inO.readLine());
						if (posicionValida(matriz, fila, columna))//Se comprueba que en la posici�n indicada se puede realizar la jugada
						{
							matriz[fila][columna] = "O";
							posicionValida = true;
						}
						else
						{
							outO.write("no\r\n");
							outO.write("LA POSICION ANTERIOR NO ES V�LIDA. Sigues jugando con  O"+"\r\n");
							outO.write(mostrarMatriz(matriz));
							outO.flush();
						}
					}
					turno++;
				}
				if(cont>=4)
				{
					if(partidaAcabada(matriz))//Se comprueba si alg�n jugador ha ganado la partida
					{
						acabada=true;
					}
				}
				cont++;
			}
			
			if(!partidaAcabada(matriz))//Se comprueba si la partida ha terminado en empate
			{
				outX.write("ok\r\n");
				outO.write("ok\r\n");
				outX.write("La partida ha acabado en empate \r\n");
				outX.write(mostrarMatriz(matriz));
				outO.write("La partida ha acabado en empate \r\n");
				outO.write(mostrarMatriz(matriz));
			}
			else if(((turno-1) % 2)==0)//Se comprueba quien es el �ltimo jugador en realizar una jugada porque ser� el ganador
			{
				outX.write("ok\r\n");
				outO.write("ok\r\n");
				outX.write("Has perdido la partida \r\n");
				outX.write(mostrarMatriz(matriz));
				outO.write("Has ganado la partida \r\n");
				outO.write(mostrarMatriz(matriz));
			}
			else
			{
				outX.write("ok\r\n");
				outO.write("ok\r\n");
				outO.write("Has perdido la partida \r\n");
				outO.write(mostrarMatriz(matriz));
				outX.write("Has ganado la partida \r\n");
				outX.write(mostrarMatriz(matriz));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(this.jugadorX!=null) {
				try {
					this.jugadorX.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(this.jugadorO!=null) {
				try {
					this.jugadorO.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//Devuelve true si la fila y la columna introducidas son v�lidas y devuelve false en caso contrario
	public boolean posicionValida(String [][]matriz, int fila, int columna)
	{
		if(fila>=0 && fila<3 && columna>=0 && columna<3)
		{
			if(matriz[fila][columna].equals(" "))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	//Devuelve true si alg�n jugador ha ganado la partida y devuelve false en caso contrario
	public boolean partidaAcabada(String [][]matriz)
	{
		int igualesCX=0;
		int igualesCO=0;
		int igualesFX=0;
		int igualesFO=0;
		//Comprobar si hay 3 en raya en alguna fila o columna
		for(int columna=0;columna<3;columna++)
		{
			igualesCX=0;
			igualesCO=0;
			igualesFX=0;
			igualesFO=0;
			for(int fila=0;fila<3;fila++)
			{
				if(matriz[fila][columna].equals("X"))//Comprobar si hay X en la COLUMNA
				{
					igualesCX++;
				}
				if(matriz[fila][columna].equals("O"))//Comprobar si hay O en la COLUMNA
				{
					igualesCO++;
				}
				if(matriz[columna][fila].equals("X"))//Comprobar si hay X en la FILA
				{
					igualesFX++;
				}
				if(matriz[columna][fila].equals("O"))//Comprobar si hay O en la FILA
				{
					igualesFO++;
				}
			}
			if(igualesCX==3 || igualesCO==3 || igualesFX==3 || igualesFO==3)
			{
				return true;
			}
		}
		
		//Comprobar si hay 3 en raya en una diagonal
		int i=0;
		int j=0;
		int DiagonalX=0;
		int DiagonalO=0;
		while(i<3)
		{
			if(matriz[i][i].equals("X"))
			{
				DiagonalX++;
			}
			if(matriz[i][i].equals("O"))
			{
				DiagonalO++;
			}
			if(DiagonalX==3 || DiagonalO==3)
			{
				return true;
			}
			j=i;
			i++;
		}
		
		//Comprobar si hay 3 en raya en la otra diagonal
		i=0;
		DiagonalX=0;
		DiagonalO=0; 
		while(i<3)
		{
			while(j>=0)
			{
				if(matriz[i][j].equals("X"))
				{
					DiagonalX++;
				}
				if(matriz[i][j].equals("O"))
				{
					DiagonalO++;
				}
				if(DiagonalX==3 || DiagonalO==3)
				{
					return true;
				}
				i++;
				j--;
			}
		}
		
		return false;	
	}
	//Crea una cadena con el estado actual del "tablero"
	public String mostrarMatriz(String [][]matriz)
	{
		String m=new String();
		int i=0;
		int j=0;
		while(i<matriz.length)
		{
			while(j<matriz.length) 
			{
				m=m+"| ";
				m=m+matriz[i][j]+" | ";
				j++;
			}
			j=0;
			i++;
			m=m+"\r\n";
		}
		return m;
	}
}
