package InterfazGrafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class InterfazGrafica extends JFrame {

	//Declaracion de vaiables
	private JPanel contentPane;
	private JTextField textBoxInformacion;
	private JTextField textBox0_0;
	private JTextField textBox0_1;
	private JSeparator separator_1;
	private JTextField textBox0_2;
	private JTextField textBox1_0;
	private JSeparator separator_2;
	private JSeparator separator_3;
	private JTextField textBox1_1;
	private JSeparator separator_4;
	private JTextField textBox1_2;
	private JSeparator separator_5;
	private JSeparator separator_6;
	private JSeparator separator_7;
	private JTextField textBox2_0;
	private JSeparator separator_8;
	private JTextField textBox2_1;
	private JSeparator separator_9;
	private JSeparator separator_10;
	private JTextField textBox2_2;
	private JSeparator separator_11;
	private JButton btEnviar;
	private int fila=3;
	private int columna=3;

	public void mostrar()
	{
		try {
			String simbolo, partida, partidaTerminada;
			int cont;

			@SuppressWarnings("resource")
			Socket client = new Socket(InetAddress.getLocalHost(), 6666);		//Creamos el socket
			BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));		//Creamos el buffer para leer lo que manda el cliente
			Writer w = new OutputStreamWriter(client.getOutputStream());	//Creamos el writer para enviar datos al cliente

			this.setVisible(true);
			
			// El jugador envia el simbolo que ha introducido
			//Listener que se activar� cuando el usuario haga click en el bot�n Enviar
			btEnviar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (manejadorEnviar()) {
						try {
							w.write(fila + "\r\n");
							w.flush();
							w.write(columna + "\r\n");
							w.flush();
							textBoxInformacion.setText("Esperando respuesta");
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
			partidaTerminada = br.readLine();
			while (!partidaTerminada.equals("ok")) {
				// Muestra con que simbolo tiene que jugar el jugador: si con X o con O
				simbolo = br.readLine();
				textBoxInformacion.setText(simbolo);

				// Muestra el estado de la partida
				cont = 0;
				String[] tablero;
				while (cont < 3) {
					partida = br.readLine();
					tablero = partida.split("|");
					//Pone en cada posicion el simbolo correspondiente, si es que hay, y evita que esa casilla pueda ser modificada
					if (cont == 0) {
						textBox0_0.setText(tablero[2]);
						if (!tablero[2].equals(" ")) {
							textBox0_0.setEditable(false);
						}
						textBox0_1.setText(tablero[8]);
						if (!tablero[8].equals(" ")) {
							textBox0_1.setEditable(false);
						}
						textBox0_2.setText(tablero[14]);
						if (!tablero[14].equals(" ")) {
							textBox0_2.setEditable(false);
						}
					}
					if (cont == 1) {
						textBox1_0.setText(tablero[2]);
						if (!tablero[2].equals(" ")) {
							textBox1_0.setEditable(false);
						}
						textBox1_1.setText(tablero[8]);
						if (!tablero[8].equals(" ")) {
							textBox1_1.setEditable(false);
						}
						textBox1_2.setText(tablero[14]);
						if (!tablero[14].equals(" ")) {
							textBox1_2.setEditable(false);
						}
					}
					if (cont == 2) {
						textBox2_0.setText(tablero[2]);
						if (!tablero[2].equals(" ")) {
							textBox2_0.setEditable(false);
						}
						textBox2_1.setText(tablero[8]);
						if (!tablero[8].equals(" ")) {
							textBox2_1.setEditable(false);
						}
						textBox2_2.setText(tablero[14]);
						if (!tablero[14].equals(" ")) {
							textBox2_2.setEditable(false);
						}
					}
					cont++;
				}	
				partidaTerminada=br.readLine();
			}
			simbolo = br.readLine();
			textBoxInformacion.setText(simbolo);	//Indica cu�l ha sido el resultado de la partida
			cont = 0;
			String[] tablero;
			//Muestra c�mo ha acabado la partida
			while (cont < 3) {
				partida = br.readLine();
				tablero = partida.split("|");
				if (cont == 0) {
					textBox0_0.setText(tablero[2]);
					if (!tablero[2].equals(" ")) {
						textBox0_0.setEditable(false);
					}
					textBox0_1.setText(tablero[8]);
					if (!tablero[8].equals(" ")) {
						textBox0_1.setEditable(false);
					}
					textBox0_2.setText(tablero[14]);
					if (!tablero[14].equals(" ")) {
						textBox0_2.setEditable(false);
					}
				}
				if (cont == 1) {
					textBox1_0.setText(tablero[2]);
					if (!tablero[2].equals(" ")) {
						textBox1_0.setEditable(false);
					}
					textBox1_1.setText(tablero[8]);
					if (!tablero[8].equals(" ")) {
						textBox1_1.setEditable(false);
					}
					textBox1_2.setText(tablero[14]);
					if (!tablero[14].equals(" ")) {
						textBox1_2.setEditable(false);
					}
				}
				if (cont == 2) {
					textBox2_0.setText(tablero[2]);
					if (!tablero[2].equals(" ")) {
						textBox2_0.setEditable(false);
					}
					textBox2_1.setText(tablero[8]);
					if (!tablero[8].equals(" ")) {
						textBox2_1.setEditable(false);
					}
					textBox2_2.setText(tablero[14]);
					if (!tablero[14].equals(" ")) {
						textBox2_2.setEditable(false);
					}
				}
				cont++;
			}	
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Create the frame.
	 */
	public InterfazGrafica() {
		setResizable(false);
		setTitle("Tres en raya");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Definici�n del panel que ir� mostrando el resultado de la partida
		JPanel panelInformacion = new JPanel();
		panelInformacion.setBounds(5, 5, 329, 67);
		contentPane.add(panelInformacion);
		panelInformacion.setLayout(new GridLayout(0, 1, 0, 0));
		
		textBoxInformacion = new JTextField();
		textBoxInformacion.setHorizontalAlignment(SwingConstants.CENTER);
		textBoxInformacion.setEditable(false);
		panelInformacion.add(textBoxInformacion);
		textBoxInformacion.setColumns(10);
		textBoxInformacion.setText("Esperando respuesta");
		
		//Definici�n del panel que mostrar� el tablero
		JPanel panelTablero = new JPanel();
		panelTablero.setBounds(5, 83, 329, 154);
		contentPane.add(panelTablero);
		panelTablero.setLayout(null);
		
		//Definici�n de cada una de las casillas que componen el tablero
		textBox0_0 = new JTextField();
		textBox0_0.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox0_0.setHorizontalAlignment(SwingConstants.CENTER);
		textBox0_0.setBounds(20, 0, 55, 34);
		panelTablero.add(textBox0_0);
		textBox0_0.setColumns(10);
		textBox0_0.setName("textBox0_0");
		
		//Listener que se activar� cuando se pulse cualquier tecla del teclado
		textBox0_0.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox0_0.getName());
			}
		});
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.BLACK);
		separator.setToolTipText("");
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(104, 0, 38, 34);
		panelTablero.add(separator);
		
		textBox0_1 = new JTextField();
		textBox0_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox0_1.setHorizontalAlignment(SwingConstants.CENTER);
		textBox0_1.setBounds(133, 0, 55, 34);
		panelTablero.add(textBox0_1);
		textBox0_1.setColumns(10);
		textBox0_1.setName("textBox0_1");
		textBox0_1.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox0_1.getName());
			}
		});
		
		separator_5 = new JSeparator();
		separator_5.setForeground(Color.BLACK);
		separator_5.setBounds(143, 45, 38, 34);
		panelTablero.add(separator_5);
		
		separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		separator_1.setBounds(213, 0, 38, 34);
		panelTablero.add(separator_1);
		separator_1.setOrientation(SwingConstants.VERTICAL);
		
		textBox0_2 = new JTextField();
		textBox0_2.setHorizontalAlignment(SwingConstants.CENTER);
		textBox0_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox0_2.setBounds(244, 0, 55, 34);
		panelTablero.add(textBox0_2);
		textBox0_2.setColumns(10);
		textBox0_2.setName("textBox0_2");
		textBox0_2.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox0_2.getName());
			}
		});
		
		separator_6 = new JSeparator();
		separator_6.setForeground(Color.BLACK);
		separator_6.setBounds(254, 45, 38, 34);
		panelTablero.add(separator_6);
		
		separator_2 = new JSeparator();
		separator_2.setForeground(Color.BLACK);
		separator_2.setBounds(30, 45, 38, 34);
		panelTablero.add(separator_2);
		
		textBox1_0 = new JTextField();
		textBox1_0.setHorizontalAlignment(SwingConstants.CENTER);
		textBox1_0.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox1_0.setBounds(20, 55, 55, 34);
		panelTablero.add(textBox1_0);
		textBox1_0.setColumns(10);
		textBox1_0.setName("textBox1_0");
		textBox1_0.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox1_0.getName());
			}
		});
		
		separator_3 = new JSeparator();
		separator_3.setForeground(Color.BLACK);
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(104, 56, 38, 34);
		panelTablero.add(separator_3);
		
		textBox1_1 = new JTextField();
		textBox1_1.setHorizontalAlignment(SwingConstants.CENTER);
		textBox1_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox1_1.setBounds(133, 55, 55, 34);
		panelTablero.add(textBox1_1);
		textBox1_1.setColumns(10);
		textBox1_1.setName("textBox1_1");
		textBox1_1.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox1_1.getName());
			}
		});
		
		separator_9 = new JSeparator();
		separator_9.setForeground(Color.BLACK);
		separator_9.setBounds(143, 101, 38, 34);
		panelTablero.add(separator_9);
		
		separator_4 = new JSeparator();
		separator_4.setForeground(Color.BLACK);
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setBounds(213, 55, 38, 34);
		panelTablero.add(separator_4);
		
		textBox1_2 = new JTextField();
		textBox1_2.setHorizontalAlignment(SwingConstants.CENTER);
		textBox1_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox1_2.setBounds(244, 55, 55, 34);
		panelTablero.add(textBox1_2);
		textBox1_2.setColumns(10);
		textBox1_2.setName("textBox1_2");
		textBox1_2.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox1_2.getName());
			}
		});
		
		separator_11 = new JSeparator();
		separator_11.setForeground(Color.BLACK);
		separator_11.setBounds(254, 100, 38, 34);
		panelTablero.add(separator_11);
		
		separator_7 = new JSeparator();
		separator_7.setForeground(Color.BLACK);
		separator_7.setBounds(30, 99, 38, 34);
		panelTablero.add(separator_7);
		
		textBox2_0 = new JTextField();
		textBox2_0.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox2_0.setHorizontalAlignment(SwingConstants.CENTER);
		textBox2_0.setBounds(20, 109, 55, 34);
		panelTablero.add(textBox2_0);
		textBox2_0.setColumns(10);
		textBox2_0.setName("textBox2_0");
		textBox2_0.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox2_0.getName());
			}
		});
		
		separator_8 = new JSeparator();
		separator_8.setOrientation(SwingConstants.VERTICAL);
		separator_8.setForeground(Color.BLACK);
		separator_8.setBounds(104, 109, 38, 34);
		panelTablero.add(separator_8);
		
		textBox2_1 = new JTextField();
		textBox2_1.setHorizontalAlignment(SwingConstants.CENTER);
		textBox2_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox2_1.setBounds(133, 109, 55, 34);
		panelTablero.add(textBox2_1);
		textBox2_1.setColumns(10);
		textBox2_1.setName("textBox2_1");
		textBox2_1.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox2_1.getName());
			}
		});
		
		separator_10 = new JSeparator();
		separator_10.setOrientation(SwingConstants.VERTICAL);
		separator_10.setForeground(Color.BLACK);
		separator_10.setBounds(213, 109, 38, 34);
		panelTablero.add(separator_10);
		
		textBox2_2 = new JTextField();
		textBox2_2.setHorizontalAlignment(SwingConstants.CENTER);
		textBox2_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textBox2_2.setBounds(244, 109, 55, 34);
		panelTablero.add(textBox2_2);
		textBox2_2.setColumns(10);
		textBox2_2.setName("textBox2_2");
		textBox2_2.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e)
			{
				manejadorTecla(e,textBox2_2.getName());
			}
		});
		
		btEnviar = new JButton("Enviar");
		btEnviar.setBounds(233, 237, 75, 23);
		contentPane.add(btEnviar);
		
	}

	//Devuelve true si previamente a pulsar en enviar se ha introducido el s�mbolo correcto y devuelve false en caso contrario
	protected boolean manejadorEnviar()
	{
		if(fila==3 || columna==3)
		{
			textBoxInformacion.setText("ERROR. Recuerda que juegas con "+ textBoxInformacion.getText().substring(textBoxInformacion.getText().length()-1));
			return false;
		}
		else
		{
			return true;
		}
	}

	//Modifica el valor de la fila y la columna si el s�mbolo introducido es correcto
	protected void manejadorTecla(KeyEvent e, String name) {
		String s=name.substring(name.lastIndexOf("x")+1);
		String []s2=s.split("_");
		if(Character.toUpperCase(e.getKeyChar())==textBoxInformacion.getText().substring(textBoxInformacion.getText().length()-1).charAt(0))	//Determinamos si el s�mbolo introducido es correcto		
		{
			fila=Integer.parseInt(s2[0]);
			columna=Integer.parseInt(s2[1]);
		}
	}
}
